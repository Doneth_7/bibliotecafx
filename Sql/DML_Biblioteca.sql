USE BibliotecaIN5BV;
GO

INSERT INTO Categoria(nombreCategoria)
VALUES ('Accion'), ('Terror'), ('Suspenso'), ('Dibujos Animados'), ('Entretenimiento'), ('Anime'), ('Romance'); 

INSERT INTO Genero(nombreGenero)
VALUES ('Trance');
INSERT INTO Genero(nombreGenero)
VALUES ('Dramatico');
INSERT INTO Genero(nombreGenero)
VALUES ('Epico');

INSERT INTO Editorial(nombreEditorial, direccion, telefono, pais)
VALUES ('VANYDENKERF','Zona 7 Calle 18 9-78','2589-2222','Guatemala');
INSERT INTO Editorial(nombreEditorial, direccion, telefono, pais)
VALUES ('SANTILLANA','Zona 1 Edificio Santillana Nivel 5 Local 221-1','2448-1248','Guatemala');
INSERT INTO Editorial(nombreEditorial, direccion, telefono, pais)
VALUES ('GRAFISA','Zona 2 Calle 13 69-31','2680-0001','Guatemala');

INSERT INTO Autor(nombreAutor, fechaNacimiento, fechaFallecimiento, nacionalidad)
VALUES ('Julio Verne', '1828-02-08', '1905-03-24', 'Frances');
INSERT INTO Autor(nombreAutor, fechaNacimiento, fechaFallecimiento, nacionalidad)
VALUES ('Sthepen Hawking', '1812-02-07', '1870-06-09', 'Ingles');
INSERT INTO Autor(nombreAutor, fechaNacimiento, fechaFallecimiento, nacionalidad)
VALUES ('Isaac Neewton', '1928-11-11', '2012-05-15', 'Ingles');
INSERT INTO Autor(nombreAutor, fechaNacimiento, fechaFallecimiento, nacionalidad)
VALUES ('Miguel Asturias','1930-05-03','2014-01-14','Guatemalteco');

INSERT INTO Rol(nombreRol)
VALUES ('Encargado')
INSERT INTO Rol(nombreRol)
VALUES ('Bibliotecario')
INSERT INTO Rol(nombreRol)
VALUES ('Clientes');


INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Poesia', 1);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Cancion', 1);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Tragedia', 2);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Comedia', 2);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Drama', 2);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Opera', 2);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Poema Epico', 3);
INSERT INTO SubGenero(nombreSubGenero, idGenero)
VALUES ('Romance', 3);

INSERT INTO Usuario(nombre, apellido, telefono, correo, direccion, nick, contrasena, idRol)
VALUES ('Emili', 'Letona','3201-2145','27997eme@gmail.com','Zona 3 Av Elena', 'Mopet', '123', 1);
INSERT INTO Usuario(nombre, apellido, telefono, correo, direccion, nick, contrasena, idRol)
VALUES ('Lizeth','Rioz','98745643','lizM@gmail.com','19 Calle  zona 9', 'Venz', 'Benzhin', 3);
INSERT INTO Usuario(nombre, apellido, telefono, correo, direccion, nick, contrasena, idRol)
VALUES ('Rezor','Kalua','21562749','kalou@gmail.com','Zona 4 Mixco Bosques de San Nicolas', 'Bang', '1234',3);
INSERT INTO Usuario(nombre, apellido, telefono, correo, direccion, nick, contrasena, idRol)
VALUES ('Dayana', 'Garcia', '95462847', 'buda@gmail.com', 'Zona 21', 'Hurricane', 'AndyBlack', 3);
INSERT INTO Usuario(nombre, apellido, telefono, correo, direccion, nick, contrasena, idRol)
VALUES ('Dailyn', 'Gomez', '6455972314', 'cDail@hotmail.com', 'Brucelas Belgica', 'MainHertz', '000', 3);



SELECT *FROM Categoria;
SELECT *FROM Genero;
SELECT *FROM Editorial;
SELECT *FROM Autor;
SELECT *FROM Rol;
SELECT *FROM SubGenero;
SELECT *FROM Usuario;
