CREATE DATABASE BibliotecaIN5BV;

GO


USE BibliotecaIN5BV;
GO




CREATE TABLE Genero (
	idGenero INT IDENTITY (1,1) NOT NULL,
	nombreGenero VARCHAR (50) NOT NULL,
	PRIMARY KEY (idGenero)
);


CREATE TABLE Editorial (
	idEditorial INT IDENTITY (1,1) NOT NULL,
	nombreEditorial VARCHAR (50) NOT NULL,
	direccion VARCHAR (50) NOT NULL,
	telefono VARCHAR (50) NOT NULL,
	pais VARCHAR (50) NOT NULL,
	PRIMARY KEY (idEditorial)
);



CREATE TABLE Categoria (
	idCategoria INT IDENTITY (1,1) NOT NULL,
	nombreCategoria VARCHAR (50) NOT NULL,
	PRIMARY KEY (idCategoria)
);

CREATE TABLE Rol (
	idRol INT IDENTITY (1,1) NOT NULL,
	nombreRol VARCHAR (50) NOT NULL,
	PRIMARY KEY (idRol)
);

CREATE TABLE Autor (
	idAutor INT IDENTITY (1,1) NOT NULL,
	nombreAutor VARCHAR (50) NOT NULL,
	fechaNacimiento DATE NOT NULL,
	fechaFallecimiento DATE NOT NULL,
	nacionalidad VARCHAR (50) NOT NULL,
	PRIMARY KEY (idAutor)
);


CREATE TABLE SubGenero (
	idSubGenero INT IDENTITY (1,1) NOT NULL,
	nombreSubGenero VARCHAR (50) NOT NULL,
	idGenero INT NOT NULL,
	PRIMARY KEY (idSubGenero),
	FOREIGN KEY (idGenero) REFERENCES Genero (idGenero)
);


CREATE TABLE Usuario (
	idUsuario INT IDENTITY (1,1) NOT NULL,
	nombre VARCHAR (50) NOT NULL,
	apellido VARCHAR (50) NOT NULL,
	telefono VARCHAR (50) NOT NULL,
	correo VARCHAR (50) NOT NULL,
	direccion VARCHAR (50) NOT NULL,
	nick VARCHAR (50) NOT NULL,
	contrasena VARCHAR (50) NOT NULL,
	idRol INT NOT NULL,
	PRIMARY KEY (idUsuario),
	FOREIGN KEY (idRol) REFERENCES Rol (idRol)
);


CREATE TABLE Libro (
	idLibro INT IDENTITY (1,1) NOT NULL,
	titulo VARCHAR (50) NOT NULL,
	precio DECIMAL NOT NULL,
	descripcion VARCHAR (100) NOT NULL,
	idAutor INT NOT NULL,
	idGenero INT NOT NULL,
	idCategoria INT NOT NULL,
	idEditorial INT NOT NULL,
	PRIMARY KEY (idLibro),
	FOREIGN KEY (idAutor) REFERENCES Autor (idAutor),
	FOREIGN KEY (idGenero) REFERENCES Genero (idGenero),
	FOREIGN KEY (idCategoria) REFERENCES Categoria (idCategoria),
	FOREIGN KEY (idEditorial) REFERENCES Editorial (idEditorial)
);

CREATE TABLE Prestamo (
	idPrestamo INT IDENTITY (1,1) NOT NULL,
	idLibro INT NOT NULL,
	idUsuario INT NOT NULL,
	fechaPrestamo DATE NOT NULL,
	fechaDevolucion DATE NOT NULL,
	multa DECIMAL NOT  NULL,
	PRIMARY KEY (idPrestamo),
	FOREIGN KEY (idLibro) REFERENCES Libro (idLibro),
	FOREIGN KEY (idUsuario) REFERENCES Usuario (idUsuario)
);

CREATE TABLE Compra (
	idCompra INT IDENTITY (1,1) NOT NULL,
	idLibro INT NOT NULL,
	idUsuario INT NOT NULL,
	fechaCompra DATE NOT NULL,
	PRIMARY KEY (idCompra),
	FOREIGN KEY (idLibro) REFERENCES libro (idLibro),
	FOREIGN KEY (idUsuario) REFERENCES Usuario (idUsuario) 
);


SELECT * FROM Usuario

SELECT * FROM Editorial

DELETE FROM Editorial WHERE idEditorial > 4