
GO 

USE BibliotecaIN5BV;

GO



CREATE VIEW c_Categoria

AS

SELECT * FROM Categoria;

SELECT* FROM c_Categoria

CREATE VIEW g_Genero

AS

SELECT* FROM Genero;

SELECT* FROM g_Genero

CREATE VIEW e_Editorial

AS

SELECT * FROM Editorial;


CREATE VIEW r_Rol
AS
SELECT * FROM Rol;


CREATE VIEW a_autor
AS
SELECT * FROM Autor;

CREATE VIEW s_SubGenero
AS 
SELECT * FROM SubGenero;


CREATE VIEW u_Usuario
AS
SELECT* FROM Usuario;


SELECT * FROM u_Usuario