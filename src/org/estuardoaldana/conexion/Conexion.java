package org.estuardoaldana.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Estuardo Aldana ©
 */
public class Conexion {
    private static Conexion instancia;
    private Connection connection;
    private Statement statement;
    private String connectionString = "jdbc:sqlserver://192.168.107.158:1433;"
            + "databaseName=BibliotecaIN5BV;"
            + "user=in5bv;"
            + "password=1234;";
    
    public Conexion() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            connection = DriverManager.getConnection(connectionString);
            statement = connection.createStatement();
        } catch(ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (InstantiationException ex) {
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public static Conexion getInstancia() {
        if(instancia == null) {
            instancia = new Conexion();
        }
        return instancia;
    }
    
    public ResultSet consultar(String consulta) {
        ResultSet resultado = null;
        
        try {
            resultado = statement.executeQuery(consulta);
        } catch(SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return resultado;
    }

    public void ejecutar(String sql) {
        try {
            statement.execute(sql);
        } catch(SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    
    
}
