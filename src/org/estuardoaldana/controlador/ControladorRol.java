/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.controlador;

import java.sql.ResultSet;
import java.util.ArrayList;
import org.estuardoaldana.bean.Rol;
import org.estuardoaldana.conexion.Conexion;

/**
 *
 * @author informatica
 */
public class ControladorRol {
    private static ControladorRol instancia;
    private ArrayList<Rol> rols = new ArrayList<>();
    
    public static ControladorRol getInstancia(){
        if(instancia == null){        
           instancia = new ControladorRol();
        }
            return instancia;
    }
    
    public ArrayList<Rol> getRols(){
        String sql = "SELECT * FROM Rol";
        ResultSet resultados = Conexion.getInstancia().consultar(sql);
        try{
            if(resultados != null){
                while(resultados.next()){
                    Rol rol = new Rol();
                    rol.setIdRol(resultados.getInt("idRol"));
                    rol.setNombreRol(resultados.getString("nombreRol"));
                    rols.add(rol);
                }             
            }         
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        } 
        return rols;
    }
    
    public void crearRol(Rol rol){
        String sql = "INSERT INTO Rol(nombreRol)" + "VALUES ('"+rol.getNombreRol()+"')";
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);    
    }
    
    public void editarRol(Rol rol){
        String sql = "UPDATE Rol SET "
                + "nombreRol='"+ rol.getNombreRol()+"'"
                + "WHERE idRol = " + rol.getIdRol();
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void eliminarRol(int idRol) {
        String sql = "DELETE FROM Rol WHERE idRol=" + idRol;
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
}
