package org.estuardoaldana.controlador;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.estuardoaldana.bean.Categoria;
import org.estuardoaldana.conexion.Conexion;

/**
 *
 * @author Estuardo Aldana ©
 */
public class ControladorCategoria {
    private static ControladorCategoria instancia;
    private ArrayList<Categoria> categorias = new ArrayList<>();
    
    public static ControladorCategoria getInstancia() {
        if(instancia == null) {
            instancia = new ControladorCategoria();
        }
        return instancia;
    }

    public ArrayList<Categoria> getCategorias() {
        String sql = "SELECT * FROM Categoria";
        ResultSet resultados = Conexion.getInstancia().consultar(sql);
        try {
            if(resultados != null) {
                while(resultados.next()) {
                    Categoria categoria = new Categoria();
                    categoria.setIdCategoria(resultados.getInt("idCategoria"));
                    categoria.setNombreCategoria(resultados.getString("nombreCategoria"));
                    categorias.add(categoria);
                }
            }
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        return categorias;
    }
    
    
    public void crearCategoria(Categoria categoria) {
        String sql = "INSERT INTO Categoria(nombreCategoria) "
                + "VALUES ('"+categoria.getNombreCategoria()+"')";
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void editarCategoria(Categoria categoria) {
        String sql = "UPDATE Categoria SET "
                + "nombreCategoria='"+categoria.getNombreCategoria()+"'"
                + "WHERE idCategoria = " + categoria.getIdCategoria();
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void eliminarCategoria(int idCategoria) {
        String sql = "DELETE FROM Categoria WHERE idCategoria=" + idCategoria;
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
}
