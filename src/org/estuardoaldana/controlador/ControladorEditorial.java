/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.controlador;

import java.sql.ResultSet;
import java.util.ArrayList;
import org.estuardoaldana.bean.Editorial;
import org.estuardoaldana.conexion.Conexion;


/**
 *
 * @author informatica
 */


public class ControladorEditorial {
    private static ControladorEditorial instancia;
    private ArrayList<Editorial> editoriales = new ArrayList<>();
    
    public static ControladorEditorial getInstancia() {
        if(instancia == null) {
            instancia = new ControladorEditorial();
        }
        return instancia;
    }
    
    public void agregarEditorial(Editorial editorial) {
        String sql = "INSERT INTO Editorial(nombreEditorial, direccion, telefono, pais)"
                + "VALUES('"+editorial.getnombreEditorial()+"',"
                + "'"+editorial.getdireccion()+"',"
                + "'"+editorial.gettelefono()+"',"
                + "'"+editorial.getpais()+"')";
        
        Conexion.getInstancia().ejecutar(sql);
    }
      
    public ArrayList<Editorial> getEditoriales() {
        String sql = "SELECT * FROM Editorial";
        ResultSet resultados = Conexion.getInstancia().consultar(sql);
        try {
            if(resultados != null) {
                while(resultados.next()) {
                    Editorial editorial = new Editorial();
                    editorial.setidEditorial(resultados.getInt("idEditorial"));
                    editorial.setnombreEditorial(resultados.getString("nombreEditorial"));
                    editorial.setdireccion(resultados.getString("direccion"));
                    editorial.settelefono(resultados.getString("telefono"));
                    editorial.setpais(resultados.getString("pais"));
                    
                    System.out.println(editorial.getidEditorial());
                    System.out.println(editorial.getnombreEditorial());
                    System.out.println(editorial.getdireccion());
                    System.out.println(editorial.getpais());
                    System.out.println(editorial.gettelefono());
                    System.out.println("***********************************");
                    editoriales.add(editorial);
                }
            }
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        return editoriales;
    }   
        
    public void crearEditorial(Editorial editorial) {
        String sql = "INSERT INTO Editorial(nombreEditorial, direccion, telefono, pais)"
                + "VALUES('"+editorial.getnombreEditorial()+"',"
                + "'"+editorial.getdireccion()+"',"
                + "'"+editorial.gettelefono()+"',"
                + "'"+editorial.getpais()+"')";
        
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void editarEditorial(Editorial editorial) {
        String sql = "UPDATE Editorial SET "
                + "nombreEditorial='"+editorial.getnombreEditorial()+"'"
                +"direccion='"+editorial.getdireccion()+"'"
                +"telefono='"+editorial.gettelefono()+"'"
                +"pais='"+editorial.getpais()+"'"
                + "WHERE idEditorial = " + editorial.getidEditorial();
        
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    } 
    
    public void eliminarEditorial(int idEditorial) {
        String sql = "DELETE FROM Editorial WHERE idEditorial=" + idEditorial;
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
}

