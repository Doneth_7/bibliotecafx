package org.estuardoaldana.controlador;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.estuardoaldana.bean.Rol;
import org.estuardoaldana.bean.Usuario;
import org.estuardoaldana.conexion.Conexion;

/**
 *
 * @author Javier Huertas
 */
public class ControladorUsuario {
    private static ControladorUsuario instancia;
    private Usuario usuario;
    
    public static ControladorUsuario getInstancia() {
        if(instancia == null) {
            instancia = new ControladorUsuario();
        } 
        return instancia;
    }
    
    public boolean autenticarUsuario(String nick, String contrasena) {
        String consulta = "SELECT * FROM Usuario "
                + "WHERE nick = '" + nick + "' AND contrasena = '"+contrasena+"'";
        boolean acceso = false;
        ResultSet resultado = Conexion.getInstancia().consultar(consulta);
        
        if(resultado != null) {
            try {
                while(resultado.next()) {
                    usuario = new Usuario();
                    usuario.setIdUsuario(resultado.getInt("idUsuario"));
                    usuario.setApellido(resultado.getString("apellido"));
                    usuario.setNombre(resultado.getString("nombre"));
                    usuario.setCorreo(resultado.getString("correo"));
                    usuario.setNick(resultado.getString("nick"));
                    usuario.setContrasena(resultado.getString("contrasena"));
                    usuario.setRol(new Rol(resultado.getInt("idRol"), ""));
                    System.out.println("Si Existe usuario!");
                    acceso = true;
                }
            } catch(SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return acceso;
    }
    
    public void crearEditorial(Usuario usuario) {
        String sql = "INSERT INTO Usuario(nombre, apellido, telefono, correo, direccion, nick, correo, contrasena)"
                + "VALUES('"+usuario.getNombre()+"',"
                +"apellido='"+usuario.getApellido()+"'"
                +"telefono='"+usuario.getTelefono()+"'"
                +"correo='"+usuario.getCorreo()+"'"
                +"direccion='"+usuario.getDireccion()+"'"
                +"nick='"+usuario.getNick()+"'"
                +"contrasena='"+usuario.getContrasena()+"'";
                
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void editarUsuario(Usuario usuario) {
        String sql = "UPDATE Usuario SET "
                + "nombre='"+usuario.getNombre()+"'"
                +"apellido='"+usuario.getApellido()+"'"
                +"telefono='"+usuario.getTelefono()+"'"
                +"correo='"+usuario.getCorreo()+"'"
                +"direccion='"+usuario.getDireccion()+"'"
                +"nick='"+usuario.getNick()+"'"
                +"contrasena='"+usuario.getContrasena()+"'"
                + "WHERE idCategoria = " + usuario.getIdUsuario();
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void eliminarUsuario(int idUsuario) {
        String sql = "DELETE FROM Usuario WHERE idUsuario=" + idUsuario;
        System.out.println(sql);
        Conexion.getInstancia().ejecutar(sql);
    }
    
}
