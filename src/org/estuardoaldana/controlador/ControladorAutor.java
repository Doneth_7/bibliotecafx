package org.estuardoaldana.controlador;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.estuardoaldana.bean.Autor;
import org.estuardoaldana.conexion.Conexion;

/**
 *
 * @author Estuardo Aldana ©
 * 
 */
public class ControladorAutor {
    private static ControladorAutor instancia;
    private ArrayList<Autor> autores = new ArrayList<>();
    
    public static ControladorAutor getInstancia() {
        if(instancia == null) {
            instancia = new ControladorAutor();
        }
        return instancia;
    }
    
    /*
    CRUD
    CREATE
    READ == SELECT
    UPDATE
    DELETE
    */
    
    public void agregarAutor(Autor autor) {
        String sql = "INSERT INTO Autor(nombreAutor, fechaNacimiento, fechaFallecimiento, nacionalidad)"
                + "VALUES('"+autor.getNombreAutor()+"',"
                + "'"+autor.getFechaNacimiento()+"',"
                + "'"+autor.getFechaFallecimiento()+"',"
                + "'"+autor.getNacionalidad()+"')";
        
        Conexion.getInstancia().ejecutar(sql);
    }
    
    public void crearAutor(Autor autor) {
        try {
            CallableStatement sp_agregarAutor = 
                    Conexion.getInstancia().getConnection().prepareCall("{call sp_agregarAutor(?,?,?,?)}");
            sp_agregarAutor.setString("nombreAutor", autor.getNombreAutor());
            sp_agregarAutor.setDate("fechaNacimiento", autor.getFechaNacimiento());
            sp_agregarAutor.setDate("fechaFallecimiento", autor.getFechaFallecimiento());
            sp_agregarAutor.setString("nacionalidad", autor.getNacionalidad());
            sp_agregarAutor.execute();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public ArrayList<Autor> getAutores() {
        String sql = "SELECT * FROM Autor";
        ResultSet resultados = Conexion.getInstancia().consultar(sql);
        try {
            if(resultados != null) {
                while(resultados.next()) {
                    Autor autor = new Autor();
                    autor.setIdAutor(resultados.getInt("idAutor"));
                    autor.setNombreAutor(resultados.getString("nombreAutor"));
                    autor.setFechaNacimiento(resultados.getDate("fechaNacimiento"));
                    autor.setFechaFallecimiento(resultados.getDate("fechaFallecimiento"));
                    autor.setNacionalidad(resultados.getString("nacionalidad"));
                    autores.add(autor);
                }
            }
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        return autores;
    }
    
    public void editarAutor(Autor autor) {
        try {
            CallableStatement sp_updateAutor = 
                    Conexion.getInstancia().getConnection().prepareCall("{call sp_updateAutor(?,?,?,?,?)}");
            sp_updateAutor.setInt("idAutor", autor.getIdAutor());
            sp_updateAutor.setString("nombreAutor", autor.getNombreAutor());
            sp_updateAutor.setDate("fechaNacimiento", autor.getFechaNacimiento());
            sp_updateAutor.setDate("fechaFallecimiento", autor.getFechaFallecimiento());
            sp_updateAutor.setString("nacionalidad", autor.getNacionalidad());
            sp_updateAutor.execute();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void eliminarAutor(int idAutor) {
        try {
            CallableStatement sp_deleteAutor = 
                    Conexion.getInstancia().getConnection().prepareCall("{call sp_deleteAutor(?)}");
            sp_deleteAutor.setInt("idAutor", idAutor);
            sp_deleteAutor.execute();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
