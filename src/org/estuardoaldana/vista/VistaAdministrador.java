package org.estuardoaldana.vista;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.estuardoaldana.catalogo.CatalogoAutor;
import org.estuardoaldana.catalogo.CatalogoCategoria;
import org.estuardoaldana.catalogo.CatalogoEditorial;
import org.estuardoaldana.catalogo.CatalogoRol;

/**
 *
 * @author Estuardo Aldana ©
 */
public class VistaAdministrador {
    private static VistaAdministrador instancia;
    private Stage pantalla;
    private TabPane contenedorTabs;
    private Tab tabAutor;
    private Tab tabCategoria;
    private Tab tabRol;
    private Tab tabEditorial;
    
    
    public static VistaAdministrador getInstancia() {
        if(instancia == null) {
            instancia = new VistaAdministrador();
        }
        return instancia;
    }

    public void mostrarVistaUsuario(Stage stage) {
        this.pantalla = stage;
        contenedorTabs = new TabPane();
        tabAutor = new Tab("Autor");
        tabCategoria = new Tab("Categoria");
        tabRol = new Tab("Rol");
        tabEditorial = new Tab("Editorial");
        
        tabAutor.setContent(CatalogoAutor.getInstancia().getContenido());
        tabCategoria.setContent(CatalogoCategoria.getInstancia().getContenido());
        tabRol.setContent(CatalogoRol.getInstancia().getContenido());
        tabEditorial.setContent(CatalogoEditorial.getInstancia().getContenido());
        
        
        contenedorTabs.getTabs().addAll(tabAutor,tabCategoria, tabRol, tabEditorial);
        contenedorTabs.setId("fondo");
        VBox vboxPrincipal = new VBox(10);
        vboxPrincipal.getChildren().add(contenedorTabs);
        
        Scene scene = new Scene(vboxPrincipal, 640, 372);
        scene.getStylesheets().add("/org/estuardoaldana/resources/css/biblioteca.css");
        pantalla.setScene(scene);
        pantalla.setTitle("Biblioteca");
        pantalla.show();
    }
    
}
