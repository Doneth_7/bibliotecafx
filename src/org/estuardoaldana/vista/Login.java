package org.estuardoaldana.vista;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.estuardoaldana.controlador.ControladorUsuario;

/**
 *
 * @author Estuardo Aldana ©
 */
public class Login {
    private static Login instancia;
    private Stage pantalla;
    
    public static Login getInstancia() {
        if(instancia == null) {
            instancia = new Login();
        }
        return instancia;
    }
    
    public void mostrarLogin(Stage stage) {
        this.pantalla = stage;
        
        GridPane gridPane = new GridPane();
        gridPane.setId("gridPane");
        gridPane.setPadding(new Insets(20));
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setAlignment(Pos.CENTER);
        
        gridPane.setStyle("gridPane");
        
        Text titulo = new Text("Bienvenido");
        titulo.setId("titulo");
        gridPane.add(titulo, 1, 0);
        
       /* Label labelNick = new Label("Nick: ");
        labelNick.setId("label");
        gridPane.add(labelNick, 1, 1); 
               */
        
        TextField textField = new TextField();
        textField.setPromptText("Usuario: ");
        gridPane.add(textField, 1, 1);
        
        /*Label labelContrasena = new Label("Contraseña: ");
        labelContrasena.setId("label");
        gridPane.add(labelContrasena, 0, 2);*/
        
        PasswordField passwordField = new PasswordField();
        gridPane.add(passwordField, 1, 2);
        passwordField.setPromptText("Contraseña: ");
    
        
        Button btn = new Button("Iniciar Sesion");
        btn.setDefaultButton(true);
        btn.setId("button");
        gridPane.add(btn, 1, 3);
       
        
        Label error = new Label();
        gridPane.add(error, 1, 4);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String nick = textField.getText();
                String contrasena = passwordField.getText();
                boolean acceso = ControladorUsuario.getInstancia().autenticarUsuario(nick, contrasena);
                
                if(acceso == true) {
                    VistaAdministrador.getInstancia().mostrarVistaUsuario(pantalla);
                } else {
                    error.setText("Usuario y/o contraseña incorrecto");
                }
            }
        });

        Scene scene = new Scene(gridPane, 800, 470);
        scene.getStylesheets().add("/org/estuardoaldana/resources/css/biblioteca.css");
        pantalla.setScene(scene);
        pantalla.setTitle("ProyectoFinal");
        pantalla.show();
    }
}
