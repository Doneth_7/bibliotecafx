/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.estuardoaldana.principal;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.estuardoaldana.vista.Login;
import org.estuardoaldana.vista.VistaAdministrador;

/**
 *
 * @author Estuardo Aldana ©
 */
public class Principal extends Application {
    
    @Override
    public void start(Stage primaryStage) {
       Login.getInstancia().mostrarLogin(primaryStage);
       //VistaAdministrador.getInstancia().mostrarVistaUsuario(primaryStage);
       primaryStage.getIcons().add(new Image("/org/estuardoaldana/resources/css/time.jpg"));

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
       
    }
    
}
