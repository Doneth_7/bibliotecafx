/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.catalogo;

import java.awt.Insets;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.estuardoaldana.bean.Usuario;
import org.estuardoaldana.controlador.ControladorUsuario;

/**
 *
 * @author informatica
 */
public class CatalogoUsuario {
    private static CatalogoUsuario instancia;
    private TableColumn<Usuario, Integer> colIdUsuario;
    private TableColumn<Usuario, String> colCorreo;
    private TableColumn<Usuario, String> colNombre;
    private TableColumn<Usuario, String> colApellido;
    private TableColumn<Usuario, String> colTelefono;
    private TableColumn<Usuario, String> colDireccion;
    private TableColumn<Usuario, String> colNick;
    private TableColumn<Usuario, String> colContrasena;      
    private TableView<Usuario> tableViewUsuario;
    private ObservableList observableListUsuario;
    private TextField tfIdUsuario = new TextField();
    private TextField tfCorreo = new TextField();
    private TextField tfNombre = new TextField();
    private TextField tfApellido = new TextField();
    private TextField tfTelefono = new TextField();
    private TextField tfDireccion = new TextField();
    private TextField tfNick = new TextField();
    private TextField tfContrasena = new TextField();
    
        public static CatalogoUsuario getInstancia() {
         
        if(instancia == null) {
            instancia = new CatalogoUsuario();
        }
        return instancia;
    }
     
    public VBox getContenido() {
        VBox vbox = new VBox(15);
        observableListUsuario = FXCollections.observableArrayList(ControladorUsuario.getInstancia().getUsuarios());
        
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(15);
        gridPane.setPadding(new Insets(25));    
        
    
}
