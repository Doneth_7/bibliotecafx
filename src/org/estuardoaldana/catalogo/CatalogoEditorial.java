/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.catalogo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.estuardoaldana.bean.Editorial;
import org.estuardoaldana.controlador.ControladorEditorial;

/**
 *
 * @author informatica
 */
public class CatalogoEditorial {
    private static CatalogoEditorial instancia;
    private TableColumn<Editorial, Integer> colIdEditorial;
    private TableColumn<Editorial, String> colNombreEditorial;
    private TableColumn<Editorial, String> colDireccion;
    private TableColumn<Editorial, String> colTelefono;
    private TableColumn<Editorial, String> colPais;
    private TableView<Editorial> tableViewEditorial;
    private ObservableList observableListEditorial;
    private TextField tfIdEditorial = new TextField();
    private TextField tfNombreEditorial = new TextField();
    private TextField tfDireccion = new TextField();
    private TextField tfTelefono = new TextField();
    private TextField tfPais = new TextField();
    
     public static CatalogoEditorial getInstancia() {
         
        if(instancia == null) {
            instancia = new CatalogoEditorial();
        }
        return instancia;
    }
     
    public VBox getContenido() {
        VBox vbox = new VBox(15);
        observableListEditorial = FXCollections.observableArrayList(ControladorEditorial.getInstancia().getEditoriales());
        
        
     
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25));
        
        Label titulo = new Label("Catalogo Editorial");
        gridPane.add(titulo, 0, 0);
        colIdEditorial = new TableColumn<>("Id Editorial");
        colIdEditorial.setCellValueFactory(new PropertyValueFactory<>("idEditorial"));
        colIdEditorial.setMinWidth(100);
        colNombreEditorial = new TableColumn<>("Nombre");
        colNombreEditorial.setCellValueFactory(new PropertyValueFactory<>("nombreEditorial"));
        colNombreEditorial.setMinWidth(150);
        colDireccion = new TableColumn<>("direccion");
        colDireccion.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        colDireccion.setMinWidth(140);
        colTelefono = new TableColumn<>("telefono");
        colTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        colTelefono.setMinWidth(100);
        colPais = new TableColumn<>("Pais");
        colPais.setCellValueFactory(new PropertyValueFactory<>("pais"));
        colPais.setMinWidth(100);
         
        tableViewEditorial = new TableView();
        tableViewEditorial.getColumns().addAll(colIdEditorial, colNombreEditorial, colDireccion, colTelefono, colPais);
        tableViewEditorial.setItems(observableListEditorial);
        gridPane.add(tableViewEditorial, 0, 2);
    
        VBox formulario = new VBox(10);
        formulario.getStyleClass().add("formulario");
        Label title = new Label("Gestion Autor");
        title.getStyleClass().add("label-header");
        
        tfIdEditorial.setDisable(true);
        tfIdEditorial.setPromptText("ID Editorial");
        tfNombreEditorial.setPromptText("Nombre Editorial");
        tfDireccion.setPromptText("Direccion");
        tfTelefono.setPromptText("Telefono");
        tfPais.setPromptText("Pais");
        
        Button btnAgregar = new Button("Agregar Editorial");
        Button btnEditar = new Button("Editar Editorial");
        Button btnEliminar = new Button("Eliminar Editorial");
        
         formulario.getChildren().addAll(titulo, tfIdEditorial, tfNombreEditorial, tfDireccion, tfTelefono, 
                tfPais, btnAgregar, btnEditar, btnEliminar);

        gridPane.add(formulario, 1, 0, 1, 4);
        btnAgregar.setOnAction(new EventHandler<ActionEvent>(){ 
            @Override
            public void handle(ActionEvent event) {
                
                Editorial editorial = new Editorial(0,
                tfNombreEditorial.getText(),
                tfDireccion.getText(),
                tfTelefono.getText(),
                tfPais.getText());

                
                ControladorEditorial.getInstancia().crearEditorial(editorial);
                
                tfNombreEditorial.clear();
                tfDireccion.clear();
                tfTelefono.clear();
                tfPais.clear();
                
            }
            
        });
        
        btnEditar.setOnAction((ActionEvent event) -> {
        });
        
        btnEliminar.setOnAction((ActionEvent event) -> {
            
        });
        vbox.getChildren().add(gridPane);
        return vbox;
    }
    
}

