package org.estuardoaldana.catalogo;

import java.sql.Date;
import java.util.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.estuardoaldana.bean.Autor;
import org.estuardoaldana.bean.Categoria;
import org.estuardoaldana.controlador.ControladorAutor;
import org.estuardoaldana.controlador.ControladorCategoria;

/**
 *
 * @author Estuardo Aldana ©
 */
public class CatalogoCategoria {
    private static CatalogoCategoria instancia;
    private TableColumn<Categoria, Integer> colIdCategoria;
    private TableColumn<Categoria, String> colNombreCategoria;
    private TableView<Categoria> tableViewCategoria;
    private ObservableList<Categoria> observableListCategoria;
    private TextField tfIdCategoria = new TextField();
            
    private TextField tfNombreCategoria = new TextField();
    
    public static CatalogoCategoria getInstancia()  {
        if(instancia == null) {
            instancia = new CatalogoCategoria();
        }
        return instancia;
    }
    
    public VBox getContenido() {
        VBox vBox = new VBox();
        observableListCategoria = FXCollections.observableList(ControladorCategoria.getInstancia().getCategorias());
        
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25));
        
        Label titulo = new Label("Catalogo Autor");
        gridPane.add(titulo, 0, 0);
        
        colIdCategoria = new TableColumn<>("IdCategoria");
        colIdCategoria.setCellValueFactory(new PropertyValueFactory<>("idCategoria"));
        
        colNombreCategoria = new TableColumn<>("Nombre");
        colNombreCategoria.setCellValueFactory(new PropertyValueFactory<>("nombreCategoria"));
        
        tableViewCategoria = new TableView<>();
        tableViewCategoria.getColumns().addAll(colIdCategoria, colNombreCategoria);
        tableViewCategoria.setItems(observableListCategoria);
        
        
         
        /*FORMULARIO PARA GESTION DE AUTOR! :D */
        
        VBox formulario = new VBox(10);
        formulario.getStyleClass().add("formulario");
        Label title = new Label("Gestion Categoria");
        title.getStyleClass().add("label-header");
        
        tfIdCategoria.setPromptText("ID Categoria");
        tfNombreCategoria.setPromptText("Nombre Categoria");
        
        //"borrar
        tfNombreCategoria.clear();
        
        Button btnAgregar = new Button("Agregar Categoria");
        Button btnEditar = new Button("Editar Categoria");
        Button btnEliminar = new Button("Eliminar Categoria");
        
        //SE AGREGAN LOS CONTROLES AL FORMULARIO (VBOX)
        formulario.getChildren().addAll(titulo, tfIdCategoria, tfNombreCategoria, 
                btnAgregar, btnEditar, btnEliminar);

        gridPane.add(formulario, 1, 0, 1, 4);
        btnAgregar.setOnAction(new EventHandler<ActionEvent>(){ 
            @Override
            public void handle(ActionEvent event) {
                Categoria categoria = new Categoria();
                categoria.setNombreCategoria(tfNombreCategoria.getText());
                ControladorCategoria.getInstancia().crearCategoria(categoria);
                actualizarTabla();
                tfNombreCategoria.clear();
            }
            
        });
        btnEditar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Categoria categoria = new Categoria();
                categoria.setIdCategoria(Integer.parseInt(tfIdCategoria.getText()));
                categoria.setNombreCategoria(tfNombreCategoria.getText());
                
                ControladorCategoria.getInstancia().editarCategoria(categoria);
                actualizarTabla();
                tfIdCategoria.clear();
                tfNombreCategoria.clear();
            }
            
        });
        
        btnEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int idCategoria = Integer.parseInt(tfIdCategoria.getText());
                
                ControladorCategoria.getInstancia().eliminarCategoria(idCategoria);
                actualizarTabla();
                tfIdCategoria.clear();
                tfNombreCategoria.clear();
            }
            
        });
        
        gridPane.add(tableViewCategoria, 0, 2);
        vBox.getChildren().add(gridPane);
        return vBox;
    }
    
    public void actualizarTabla() {
        ControladorCategoria.getInstancia().getCategorias().clear();
        observableListCategoria.clear();
        observableListCategoria = FXCollections.observableList(ControladorCategoria.getInstancia().getCategorias());
        tableViewCategoria.setItems(observableListCategoria);
                
    }
    
    
}
