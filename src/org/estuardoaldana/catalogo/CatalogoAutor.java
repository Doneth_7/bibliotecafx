package org.estuardoaldana.catalogo;

import java.sql.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.estuardoaldana.bean.Autor;
import org.estuardoaldana.controlador.ControladorAutor;

/**
 *
 * @author Estuardo Aldana ©
 */
public class CatalogoAutor {
    private static CatalogoAutor instancia;
    private TableColumn<Autor, Integer> colIdAutor;
    private TableColumn<Autor, String> colNombreAutor;
    private TableColumn<Autor, String> colFechaNacimiento;
    private TableColumn<Autor, String> colFechaFallecimiento;
    private TableColumn<Autor, String> colNacionalidad;
    private TableView<Autor> tableViewAutor;
    private ObservableList observableListAutor;
    private TextField tfIdAutor = new TextField();
    private TextField tfNombre = new TextField();
    private DatePicker fechaNacimiento = new DatePicker();
    private DatePicker fechaFallecimiento = new DatePicker();
    private TextField tfNacionalidad = new TextField();
    
    public static CatalogoAutor getInstancia() {
        if(instancia == null) {
            instancia = new CatalogoAutor();
        }
        return instancia;
    }
    
    
    public VBox getContenido() {
        VBox vbox = new VBox(10);
        observableListAutor = FXCollections.observableList(ControladorAutor.getInstancia().getAutores());
        
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25));
        
        Label titulo = new Label("Catalogo Autor");
        gridPane.add(titulo, 0, 0);
        colIdAutor = new TableColumn<>("Id Autor");
        colIdAutor.setCellValueFactory(new PropertyValueFactory<>("idAutor"));
        colNombreAutor = new TableColumn<>("Nombre");
        colNombreAutor.setCellValueFactory(new PropertyValueFactory<>("nombreAutor"));
        colFechaNacimiento = new TableColumn<>("Nacimiento");
        colFechaNacimiento.setCellValueFactory(new PropertyValueFactory<>("fechaNacimiento"));
        colFechaFallecimiento = new TableColumn<>("Fallecimiento");
        colFechaFallecimiento.setCellValueFactory(new PropertyValueFactory<>("fechaFallecimiento"));
        colNacionalidad = new TableColumn<>("Nacionalidad");
        colNacionalidad.setCellValueFactory(new PropertyValueFactory<>("nacionalidad"));
        
        tableViewAutor = new TableView();
        tableViewAutor.getColumns().addAll(colIdAutor, colNombreAutor, colFechaNacimiento, colFechaFallecimiento, colNacionalidad);
        tableViewAutor.setItems(observableListAutor);
        gridPane.add(tableViewAutor, 0, 2);
        
        /*FORMULARIO PARA GESTION DE AUTOR! :D */
        
        VBox formulario = new VBox(10);
        formulario.getStyleClass().add("formulario");
        Label title = new Label("Gestion Autor");
        title.getStyleClass().add("label-header");
        
        tfIdAutor.setDisable(true);
        tfIdAutor.setPromptText("ID Autor");
        tfNombre.setPromptText("Nombre Autor");
        fechaNacimiento.setPromptText("Fecha Nacimiento");
        fechaFallecimiento.setPromptText("Fecha Fallecimiento");
        tfNacionalidad.setPromptText("Nacionalidad");
        
        Button btnAgregar = new Button("Agregar Autor");
        Button btnEditar = new Button("Editar Autor");
        Button btnEliminar = new Button("Eliminar Autor");
        
        //SE AGREGAN LOS CONTROLES AL FORMULARIO (VBOX)
        formulario.getChildren().addAll(titulo, tfIdAutor, tfNombre, fechaNacimiento, fechaFallecimiento, 
                tfNacionalidad, btnAgregar, btnEditar, btnEliminar);

        gridPane.add(formulario, 1, 0, 1, 4);
        btnAgregar.setOnAction(new EventHandler<ActionEvent>(){ 
            @Override
            public void handle(ActionEvent event) {
                
                Autor autor = new Autor(0,
                tfNombre.getText(),
                Date.valueOf(fechaNacimiento.getValue()),
                Date.valueOf(fechaFallecimiento.getValue()),
                tfNacionalidad.getText());

                
                ControladorAutor.getInstancia().crearAutor(autor);
                
                tfNombre.clear();
                tfNacionalidad.clear();
                
            }
            
        });
        btnEditar.setOnAction((ActionEvent event) -> {
        });
        
        btnEliminar.setOnAction((ActionEvent event) -> {
            
        });
        vbox.getChildren().add(gridPane);
        return vbox;
    }
    
}
