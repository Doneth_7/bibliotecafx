/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.catalogo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.estuardoaldana.bean.Categoria;
import org.estuardoaldana.bean.Rol;
import org.estuardoaldana.controlador.ControladorCategoria;
import org.estuardoaldana.controlador.ControladorRol;



/**
 *
 * @author informatica
 */
public class CatalogoRol {
    private static CatalogoRol instancia;
    private TableColumn<Rol, Integer> colIdRol; 
    private TableColumn<Rol, String> colNombreRol;
    private TableView<Rol> tableViewRol;
    private ObservableList<Rol> observableListRol;
    private TextField tfIdRol = new TextField();
    private TextField tfNombreRol = new TextField();
    
      public static CatalogoRol getInstancia()  {
        if(instancia == null) {
            instancia = new CatalogoRol();
        }
        return instancia;
    }
    
        public VBox getContenido() {
        VBox VBox = new VBox();
        observableListRol = FXCollections.observableList(ControladorRol.getInstancia().getRols());
        
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25));
        
        Label titulo = new Label("Catalogo Autor");
        gridPane.add(titulo, 0, 0);
        
        colIdRol = new TableColumn<>("IdRol");
        colIdRol.setCellValueFactory(new PropertyValueFactory<>("idRol"));
        
        colNombreRol = new TableColumn<>("Nombre");
        colNombreRol.setCellValueFactory(new PropertyValueFactory<>("nombreRol"));
        
        tableViewRol = new TableView<>();
        tableViewRol.getColumns().addAll(colIdRol, colNombreRol);
        tableViewRol.setItems(observableListRol);
        
        
        VBox formulario = new VBox(10);
        formulario.getStyleClass().add("formulario");
        Label title = new Label("Gestion Categoria");
        title.getStyleClass().add("label-header");
        
        tfIdRol.setPromptText("ID Rol");
        tfNombreRol.setPromptText("Nombre Rol");
        
        Button btnAgregar = new Button("Agregar Rol");
        Button btnEditar = new Button("Editar Rol");
        Button btnEliminar = new Button("Eliminar Rol");
        
        formulario.getChildren().addAll(titulo, tfIdRol, tfNombreRol, 
                btnAgregar, btnEditar, btnEliminar);

        gridPane.add(formulario, 1, 0, 1, 4);
        btnAgregar.setOnAction(new EventHandler<ActionEvent>(){ 
            @Override
            public void handle(ActionEvent event) {
                Rol rol = new Rol();
                rol.setNombreRol(tfNombreRol.getText());
                ControladorRol.getInstancia().crearRol(rol);
                 actualizarTabla();
                tfNombreRol.clear();
                
            }
            
        });
        
         btnEditar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Rol rol = new Rol();
                rol.setIdRol(Integer.parseInt(tfIdRol.getText()));
                rol.setNombreRol(tfNombreRol.getText());
                
                ControladorRol.getInstancia().editarRol(rol);
                actualizarTabla();
                tfIdRol.clear();
                tfNombreRol.clear();
            }
            
        });
         
         btnEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int idRol = Integer.parseInt(tfIdRol.getText());
                
                ControladorRol.getInstancia().eliminarRol(idRol);
                actualizarTabla();
                tfIdRol.clear();
                tfNombreRol.clear();
            }
            
        });
        
            gridPane.add(tableViewRol, 0, 2);
        VBox.getChildren().add(gridPane);
        return VBox;
    }
    
      public void actualizarTabla() {
        ControladorRol.getInstancia().getRols().clear();
        observableListRol.clear();
        observableListRol = FXCollections.observableList(ControladorRol.getInstancia().getRols());
        tableViewRol.setItems(observableListRol);
                
    }
    
    
}
   