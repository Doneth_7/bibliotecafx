/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.bean;

/**
 *
 * @author Informatica
 */
public class Editorial {
    
    private Integer idEditorial;
    private String nombreEditorial;
    private String direccion;
    private String telefono;
    private String pais;

    public Editorial() {
    }

    public Editorial(Integer idEditorial, String nombreEditorial, String direccion, String telefono, String pais) {
        this.idEditorial = idEditorial;
        this.nombreEditorial = nombreEditorial;
        this.direccion = direccion;
        this.telefono = telefono;
        this.pais = pais;
    }
    
    public Integer getidEditorial (){
        return idEditorial;
    }
    
    public String getnombreEditorial(){
        return nombreEditorial;
    }
    
    public String getdireccion(){
        return direccion;
    }
    
    public String gettelefono(){
        return telefono;
    }
    
    public String getpais(){
        return pais;
    }
    
    public void setidEditorial(Integer idEditorial){
        this.idEditorial = idEditorial; 
    }
    
    public void setnombreEditorial(String nombreEditorial){
        this.nombreEditorial = nombreEditorial;
    }
    
    public void setdireccion(String direccion){
        this.direccion = direccion;
    }
    
    public void settelefono(String telefono){
        this.telefono = telefono;
    }
    
    public void setpais(String pais){
        this.pais = pais;
    }
}
