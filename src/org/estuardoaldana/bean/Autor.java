package org.estuardoaldana.bean;

import java.sql.Date;

/**
 *
 * @author Estuardo Aldana ©
 */
public class Autor {
    private Integer idAutor;
    private String nombreAutor;
    private Date fechaNacimiento;
    private Date fechaFallecimiento;
    private String nacionalidad;

    public Autor() {
    }

    public Autor(Integer idAutor, String nombreAutor, Date fechaNacimiento, Date fechaFallecimiento, String nacionalidad) {
        this.idAutor = idAutor;
        this.nombreAutor = nombreAutor;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaFallecimiento = fechaFallecimiento;
        this.nacionalidad = nacionalidad;
    }

    public Integer getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaFallecimiento() {
        return fechaFallecimiento;
    }

    public void setFechaFallecimiento(Date fechaFallecimiento) {
        this.fechaFallecimiento = fechaFallecimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
}
