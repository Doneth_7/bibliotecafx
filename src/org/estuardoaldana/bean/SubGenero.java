/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.estuardoaldana.bean;

/**
 *
 * @author Informatica
 */
public class SubGenero {
    private Integer idSubGenero;
    private String nombreSubGenero;
    private Genero genero;

    public SubGenero() {
    }

    public SubGenero(Integer idSubGenero, String nombreSubGenero, Genero genero) {
        this.idSubGenero = idSubGenero;
        this.nombreSubGenero = nombreSubGenero;
        this.genero = genero;
    }

    public Integer getIdSubGenero() {
        return idSubGenero;
    }

    public void setIdSubGenero(Integer idSubGenero) {
        this.idSubGenero = idSubGenero;
    }

    public String getNombreSubGenero() {
        return nombreSubGenero;
    }

    public void setNombreSubGenero(String nombreSubGenero) {
        this.nombreSubGenero = nombreSubGenero;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }
}
